package TimShull;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.is;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @BeforeClass
    public static void startServer() throws Exception {
        Runnable serverRunner = () -> {
            try {
                String[] args = {};
                App.main(args);
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        };
        Thread serverThread = new Thread(serverRunner);
        serverThread.start();
    }

    @Test
    public void testResponseCode() {
        try {
            URL url = new URL("http://127.0.0.1:8080/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            Assert.assertThat("Test the response code", con.getResponseCode(), is(HttpURLConnection.HTTP_OK));
        } catch (MalformedURLException e) {
            Assert.fail("MalformedURLException thrown: " + e.toString());
        } catch (java.io.IOException e) {
            Assert.fail("java.io.IOException thrown: " + e.toString());
        }
    }

    @Test
    public void testResponseString() {
        try {
            URL url = new URL("http://127.0.0.1:8080/");
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            httpConnection.disconnect();
            Assert.assertEquals("<html><head></head><body>Responding to request</body></html>", content.toString());
        } catch (MalformedURLException e) {
            Assert.fail("MalformedURLException thrown: " + e.toString());
        } catch (java.io.IOException e) {
            Assert.fail("java.io.IOException thrown: " + e.toString());
        }
    }


}
