package TimShull;

import java.io.*;
import java.net.*;

public class ClientHandler extends Thread {
    private Socket socket;

    ClientHandler(Socket s) {
        socket = s;
        start();
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintStream out = new PrintStream(new BufferedOutputStream(socket.getOutputStream()));

            String s = in.readLine();
            System.out.println(s);
            out.println("HTTP/1.0 200 OK\r\n" +
                    "Content-type: text/html\r\n\r\n" +
                    "<html><head></head><body>Responding to request</body></html>\n");
            out.close();

        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
