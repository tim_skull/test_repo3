package TimShull;

import java.io.*;
import java.net.*;

public class App {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        while (true) {
            try {
                Socket s = serverSocket.accept();
                new ClientHandler(s);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }
}

